import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ProductService } from '../product.service';
import { Observable } from 'rxjs';
import { Product } from '../product';

@Component({
  selector: 'app-read-products',
  templateUrl: './read-products.component.html',
  styleUrls: ['./read-products.component.css'],
  providers: [ProductService]
})
export class ReadProductsComponent implements OnInit {

  @Output() show_create_product_event = new EventEmitter();
  @Output() show_read_one_product_event = new EventEmitter();
  @Output() show_update_product_event = new EventEmitter();
  @Output() show_delete_product_event = new EventEmitter();

  products: Product[];

  constructor(private productService: ProductService) { }

  createProduct() {
    this.show_create_product_event.emit({
      title: "Create Product"
    });
  }

  readOneProduct(id) {
    console.log(id);
    // tell the parent component (AppComponent)
    this.show_read_one_product_event.emit({
      product_id: id,
      title: "Read One Product"
    });
  }

  updateProduct(id) {
    // tell the parent component (AppComponent)
    this.show_update_product_event.emit({
      product_id: id,
      title: "Update Product"
    });
  }

  deleteProduct(id) {
    // tell the parent component (AppComponent)
    this.show_delete_product_event.emit({
      product_id: id,
      title: "Delete Product"
    });
  }

  // Read products from API
  ngOnInit() {
    this.productService.readProduct()
      .subscribe(products =>
        this.products = products['records']);
  }
}
