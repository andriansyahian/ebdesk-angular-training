import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public show_read_products_html: boolean = true
  public show_create_product_html: boolean
  public show_read_one_product_html: boolean
  public show_update_product_html: boolean
  public show_delete_product_html: any
  // public hideAll_Html;

  title = 'Read Products';
  product_id;

  // show read of a product
  showReadProduct($event) {
    // set title and product ID
    this.title = $event.title;
    this.product_id = $event.product_id;

    // hide all html then show only one html
    this.show_read_products_html = true
    this.show_create_product_html = false
    this.show_read_one_product_html = false
    this.show_update_product_html = false
    this.show_delete_product_html = false
  }

  // show create of a product
  showCreateProduct($event) {
    // set title and product ID
    this.title = $event.title;
    this.product_id = $event.product_id;

    // hide all html then show only one html
    this.show_create_product_html = true
    this.show_read_products_html = false
  }

  // show details of a product
  showReadOneProduct($event) {
    // set title and product ID
    this.title = $event.title;
    this.product_id = $event.product_id;

    // hide all html then show only one html
    this.show_read_one_product_html = true
    this.show_read_products_html = false
  }

  showUpdateProduct($event) {
    // set title and product ID
    this.title = $event.title;
    this.product_id = $event.product_id;

    this.show_update_product_html = true
    this.show_read_products_html = false
  }

  // show 'are you sure?' prompt to confirm deletion of a record
  showDeleteProduct($event) {
    // set title and product ID
    this.title = $event.title;
    this.product_id = $event.product_id;

    // hide all html then show only one html
    this.show_delete_product_html = true
    this.show_read_products_html = false
  }
}
