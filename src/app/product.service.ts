import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {catchError, map} from  'rxjs/operators';
import {Product} from './product';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _url: string = "http://localhost:1337/192.168.61.73/masterapi/product/read.php";
  constructor(private _http: Http) { }

  public readProduct(): Observable<Product[]> {
    return this._http
    .get("http://localhost:1337/192.168.12.107/masterapi/product/read.php")
    .pipe(map((res:Response)=>res.json()));
  }

  // Send product data to remote server to create it.
  createProduct(product): Observable<Product> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this._http.post(
      "http://localhost:1337/192.168.12.107/masterapi/product/create.php",
      product,
      options
    ).pipe(map((res: Response) => res.json()));
  }

  // Get a product details from remote server.
readOneProduct(product_id): Observable<Product>{
  return this._http
      .get("http://localhost:1337/192.168.12.107/masterapi/product/read_one.php?id="+product_id)
      .pipe(map((res: Response) => res.json()));
}

// Send product data to remote server to update it.
updateProduct(product): Observable<Product>{
 
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });

  return this._http.post(
      "http://localhost:1337/192.168.12.107/masterapi/product/update.php",
      product,
      options
  ).pipe(map((res: Response) => res.json()));
}

// Send product ID to remote server to delete it.
deleteProduct(product_id){
 
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });

  return this._http.post(
      "http://localhost:1337/192.168.12.107/masterapi/product/delete.php",
      { id: product_id },
      options
  ).pipe(map((res: Response) => res.json()));
}


}
