import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { Product } from './product';
import { map } from 'rxjs/operators'
import { Category } from './category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  // We need Http to talk to a remote server.
  constructor(private _http: Http) { }
 
  // Get list of categories from database via api.
  readCategories(): Observable<Category[]>{
      return this._http
          .get("http://localhost:1337/192.168.12.107/masterapi/category/read.php")
          .pipe(map((res: Response) => res.json()));
  }

}
